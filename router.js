import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const Welcome = () => import('~/pages/index').then(m => m.default || m)
const Beats = () => import('~/pages/beats/Beats').then(m => m.default || m)
const Artiste = () => import('~/pages/artiste/Profiles').then(m => m.default || m)
const Artist = () => import('~/pages/artiste/Profile').then(m => m.default || m)
const Products = () => import('~/pages/shop/Products').then(m => m.default || m)
const Product = () => import('~/pages/shop/Product').then(m => m.default || m)
const Blogs = () => import('~/pages/news/Blogs').then(m => m.default || m)
const Blog = () => import('~/pages/news/BlogPost').then(m => m.default || m)
const Checkout = () => import('~/pages/shopping/Checkout').then(m => m.default || m)

const Home = () => import('~/pages/_home').then(m => m.default || m)
const MusicLibrary = () => import('~/pages/user/music').then(m => m.default || m)
const BeatsLibrary = () => import('~/pages/user/beats').then(m => m.default || m)
const AlbumsLibrary = () => import('~/pages/user/albums').then(m => m.default || m)
const Album = () => import('~/pages/user/album').then(m => m.default || m)
const Login = () => import('~/pages/auth/login').then(m => m.default || m)
const Register = () => import('~/pages/auth/register').then(m => m.default || m)
const Verify = () => import('~/pages/auth/verify').then(m => m.default || m)
// const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
// const PasswordRequest = () => import('~/pages/auth/password/email').then(m => m.default || m)

// const Settings = () => import('~/pages/settings/index').then(m => m.default || m)
// const SettingsProfile = () => import('~/pages/settings/profile').then(m => m.default || m)
// const SettingsPassword = () => import('~/pages/settings/password').then(m => m.default || m)

const About = () => import('~/pages/static/About').then(m => m.default || m)
const Teams = () => import('~/pages/static/Team').then(m => m.default || m)
const Contact = () => import('~/pages/static/Contact').then(m => m.default || m)
const Terms = () => import('~/pages/static/Terms').then(m => m.default || m)
const Privacy = () => import('~/pages/static/Privacy').then(m => m.default || m)
const Mixing = () => import('~/pages/static/Mixing').then(m => m.default || m)
const License = () => import('~/pages/static/License').then(m => m.default || m)
const CustomBeat = () => import('~/pages/static/CustomBeat').then(m => m.default || m)
const Subscription = () => import('~/pages/subscription/Subscribe').then(m => m.default || m)
const Payment = () => import('~/pages/subscription/Payment').then(m => m.default || m)

const routes = [
  { path: '/', name: 'welcome', component: Welcome },
  { path: '/artiste', name: 'artiste', component: Artiste },
  { path: '/artiste/:slug', name: 'artist', component: Artist },
  { path: '/beat-store', name: 'beats', component: Beats },
  { path: '/shop', name: 'products', component: Products },
  { path: '/shop/:slug', name: 'product', component: Product },
  { path: '/news', name: 'blogs', component: Blogs },
  { path: '/news/:slug', name: 'blog', component: Blog },
  { path: '/checkout', name: 'checkout', component: Checkout },

  { path: '/:username/home', name: 'home', component: Home },
  { path: '/:username/music', name: 'music-library', component: MusicLibrary },
  { path: '/:username/beats', name: 'beat-library', component: BeatsLibrary },
  { path: '/:username/albums', name: 'albums-library', component: AlbumsLibrary },
  { path: '/:username/albums/:slug', name: 'album', component: Album },

  { path: '/about_us', name: 'about', component: About },
  { path: '/contact_us', name: 'contact', component: Contact },
  { path: '/our_teams', name: 'teams', component: Teams },
  { path: '/beats_license', name: 'license', component: License },
  { path: '/mixing_and_mastering', name: 'mixing', component: Mixing },
  { path: '/terms', name: 'terms', component: Terms },
  { path: '/privacy', name: 'privacy', component: Privacy },
  { path: '/custom_beats_plan', name: 'custom_beats', component: CustomBeat },
  { path: '/subscription_plans', name: 'subscriptions', component: Subscription },
  { path: '/payment', name: 'payment', component: Payment },

  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  { path: '/verify', name: 'verify', component: Verify },
  // { path: '/password/reset', name: 'password.request', component: PasswordRequest },
  // { path: '/password/reset/:token', name: 'password.reset', component: PasswordReset },

  // { path: '/:username/settings', component: Settings, children: [
  //   { path: '', redirect: { name: 'settings.profile' }},
  //   { path: 'profile', name: 'settings.profile', component: SettingsProfile },
  //   { path: 'advance', name: 'settings.advance', component: SettingsAdvance },
  //   { path: 'picture', name: 'settings.picture', component: SettingsPicture },
  //   { path: 'company', name: 'settings.company', component: SettingsCompany },
  //   { path: 'password', name: 'settings.password', component: SettingsPassword }
  // ] }
]

const scrollBehavior = (to, from, savedPosition) => {
  if (savedPosition) {
    return savedPosition
  }

  let position = {}

  if (to.matched.length < 2) {
    position = { x: 0, y: 0 }
  } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
    position = { x: 0, y: 0 }
  } if (to.hash) {
    position = { selector: to.hash }
  }

  return position
}

export function createRouter () {
  return new Router({
    routes,
    scrollBehavior,
    mode: 'history'
  })
}
