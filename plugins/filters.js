import Vue from 'vue'
import { currency } from "./currency"


Vue.filter('currency', currency)

Vue.filter('discount', function(value) {
    return value + '% OFF';
})

Vue.filter('price', function(item) {
    return '$' + item.price;
})

Vue.filter('discountedPrice', function(item) {
  return '$' + (parseFloat(item.price) * parseFloat(((100 - item.discount)/100))).toFixed(2)
})

Vue.filter('size', function(item) {
  var sizes = ''
  for (var i = 0, l = item.size.length; i < l; ++i) {
      if (sizes === '') {
        sizes += item.size[i]
      }
      sizes += ' / ' + item.size[i]
  }
    return sizes
})

Vue.filter('color', function(item) {
  var colors = ''
  for (var i = 0, l = item.color.length; i < l; ++i) {
      if (colors === '') {
        colors += item.color[i]
      }
      colors += ' / ' + item.color[i]
  }
    return colors
})