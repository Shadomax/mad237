import axios from 'axios'
import Cookies from 'js-cookie'

// state
export const state = () => ({
  headers: [
          {
            text: 'Cover',
            // align: 'left',
            sortable: false,
            value: 'cover'
          },
          { text: 'Producer', value: 'producer' },
          { text: 'Genre', value: 'genre' },
          { text: 'Title', value: 'title' },
          { text: 'BPM', value: 'bpm' },
          { text: 'Length', value: 'length' },
          // { text: 'Share', value: 'share' },
          { text: 'Price', value: 'price' },
          { text: 'Action', value: 'action' }
        ],
        beatPlans: [
          {
            name: 'NON-EXCLUSIVE',
            price: '4.99',
            id: '1'
          },{
            name: 'PREMIUM + WAV',
            price: '19.99',
            id: '2'
          },{
            name: 'PREMIUM + STEMS',
            price: '99.00',
            id: '3'
          },{
            name: 'EXCLUSIVE',
            price: '399.00',
            id: '4'
          }
        ],
        items: [
          {
            value: false,
            color: 'purple',
            name: 'Mp3 Lease',
            price: 27,
            buy_3: 'Buy 3, Get 1 FREE',
            buy_5: 'Buy 5, Get 2 FREE'
          },
          {
            value: false,
            color: 'blue',
            name: 'Premium Lease',
            price: 57,
            buy_3: 'Buy 3, Get 1 FREE',
            buy_5: 'Buy 5, Get 2 FREE'
          },
          {
            value: false,
            color: 'amber',
            name: 'Unlimited Lease',
            price: 97,
            buy_3: 'Buy 3, Get 1 FREE',
            buy_5: 'Buy 5, Get 2 FREE'
          },
          {
            value: false,
            color: 'green',
            name: 'Exclusive',
            price: '497+',
            buy_3: 'Buy 3, Get 1 FREE',
            buy_5: 'Buy 5, Get 2 FREE'
          }
        ],
        licenses: [
          {
            value: false,
            color: 'purple',
            name: 'Free',
            price: 'Free',
            items: [
              {
                icon: 'fa-check-circle',
                text: '128kbps .mp3 file',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: '.WAV file',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Vocal Tags Removed',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Stems(TrackOuts)',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'No Commercial Usage',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Cannot Distribute',
                color: 'red'
              },
              {
                icon: 'fa-check-circle',
                text: 'Credits must be given as follows "Beats produced by KovapotBeats (www.mad237.com)"',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: 'No Sync License Included',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'No License Agreement Included',
                color: 'red'
              },
              {
                icon: 'fa-check-circle',
                text: 'Instant Download',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: 'Royalty Free',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Beat Cannot Be Resold By Mad237',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Performance',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Promo',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Radio Play',
                color: 'red'
              }
            ]
          },
          {
            value: false,
            color: 'blue',
            name: 'Mp3 Lease',
            price: 'Free',
            items: [
              {
                icon: 'fa-check-circle',
                text: '128kbps .mp3 file',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: '.WAV file',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Vocal Tags Removed',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Stems(TrackOuts)',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'No Commercial Usage',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Cannot Distribute',
                color: 'red'
              },
              {
                icon: 'fa-check-circle',
                text: 'Credits must be given as follows "Beats produced by KovapotBeats (www.mad237.com)"',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: 'No Sync License Included',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'No License Agreement Included',
                color: 'red'
              },
              {
                icon: 'fa-check-circle',
                text: 'Instant Download',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: 'Royalty Free',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Beat Cannot Be Resold By Mad237',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Performance',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Promo',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Radio Play',
                color: 'red'
              }
            ]
          },
          {
            value: false,
            color: 'yellow',
            name: 'Premium Lease',
            price: '57$',
            items: [
              {
                icon: 'fa-check-circle',
                text: '128kbps .mp3 file',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: '.WAV file',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Vocal Tags Removed',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Stems(TrackOuts)',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'No Commercial Usage',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Cannot Distribute',
                color: 'red'
              },
              {
                icon: 'fa-check-circle',
                text: 'Credits must be given as follows "Beats produced by KovapotBeats (www.mad237.com)"',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: 'No Sync License Included',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'No License Agreement Included',
                color: 'red'
              },
              {
                icon: 'fa-check-circle',
                text: 'Instant Download',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: 'Royalty Free',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Beat Cannot Be Resold By Mad237',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Performance',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Promo',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Radio Play',
                color: 'red'
              }
            ]
          },
          {
            value: false,
            color: 'green',
            name: 'Unlimited Lease',
            price: '97$',
            items: [
              {
                icon: 'fa-check-circle',
                text: '128kbps .mp3 file',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: '.WAV file',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Vocal Tags Removed',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Stems(TrackOuts)',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'No Commercial Usage',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Cannot Distribute',
                color: 'red'
              },
              {
                icon: 'fa-check-circle',
                text: 'Credits must be given as follows "Beats produced by KovapotBeats (www.mad237.com)"',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: 'No Sync License Included',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'No License Agreement Included',
                color: 'red'
              },
              {
                icon: 'fa-check-circle',
                text: 'Instant Download',
                color: 'green'
              },
              {
                icon: 'fa-times-circle',
                text: 'Royalty Free',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Beat Cannot Be Resold By Mad237',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Performance',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Promo',
                color: 'red'
              },
              {
                icon: 'fa-times-circle',
                text: 'Radio Play',
                color: 'red'
              }
            ]
          },
          {
            value: false,
            color: 'amber',
            name: 'Exclusive',
            price: '497 - 999$',
            items: [
              {
                icon: 'fa-check-circle',
                text: '128kbps .mp3 file',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: '.WAV file',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Vocal Tags Removed',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Stems(TrackOuts)',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'No Commercial Usage',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Cannot Distribute',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Credits must be given as follows "Beats produced by KovapotBeats (www.mad237.com)"',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'No Sync License Included',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'No License Agreement Included',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Instant Download',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Royalty Free',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Beat Cannot Be Resold By Mad237',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Performance',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Promo',
                color: 'green'
              },
              {
                icon: 'fa-check-circle',
                text: 'Radio Play',
                color: 'green'
              }
            ]
          }
    ],
  	beats: [],
  	beat: {},
  	meta: {},
  	filteredBeats: []
})

// getters
export const getters = {
  headers: state => state.headers,
  beatPlans: state => state.beatPlans,
  items: state => state.items,
  licenses: state => state.licenses,
  beats: state => state.beats,
  beat: state => state.beat,
  meta: state => state.meta,
  filteredBeats: state => state.filteredBeats
}

// mutations
export const mutations = {

  FETCH_BEATS_SUCCESS (state, beats) {
    state.beats = beats
  },

  FETCH_BEATS_FAILURE (state) {
  	state.beats = []
  },

  FETCH_MORE_SUCCESS (state, beats) {
  	state.beats = beats
    state.filteredBeats = beats
  },

  SET_FILTERED_BEATS_SUCCESS (state, beats) {
  	state.filteredBeats = beats
  },

  SET_FILTERED_BEATS_FAILURE (state) {
  	state.filteredBeats = []
  },

  SET_META (state, meta) {
  	state.meta = meta
  },

  FETCH_BEAT_SUCCESS (state, beat) {
  	state.beat = beat
  },

  FETCH_BEAT_FAILURE (state) {
  	state.beat = {}
  }
}

// actions
export const actions = {
  async fetchBeats ({commit, dispatch}) {
  	try {
      let response = await axios.get(process.env.musicApi + '/get/beats')
      commit('FETCH_BEATS_SUCCESS', response.data.data)
      commit('SET_FILTERED_BEATS_SUCCESS', response.data.data)
      commit('SET_META', response.data.meta.pagination)
    } catch (e) { 
    	commit('FETCH_BEATS_FAILURE')
    }
  },

  async fetchMoreBeats ({commit, dispatch, state}, {url}) {
  	try {
     let response =  await axios.get(url)
     dispatch('mergeBeats', {data: response.data.data})
     commit('SET_META', response.data.meta.pagination)
    } catch (e) { 
    	console.log(e)
    	commit('FETCH_BEATS_FAILURE')
    }
  },

  mergeBeats ({commit, dispatch, state}, {data}) {
    try {
    	const result = state.beats.concat(data)
    	commit('FETCH_MORE_SUCCESS', result)
    } catch(e) {
    	// statements
    	console.log(e)
    	commit('FETCH_BEATS_FAILURE')
    }
  },

  filterBeats ({commit, dispatch, state}, {letter}) {
  	try {
  		let filtereBeats = state.beats.filter(beat => {
           return beat.stageName.toLowerCase().indexOf(letter.toLowerCase()) > -1
    	})

    	commit('SET_FILTERED_BEATS_SUCCESS', filtereBeats)
  	} catch(e) {
  		console.log(e)
  		commit('SET_FILTERED_BEATS_FAILURE')
  	}
  },

  async fetchBeat ({commit, dispatch, state}, {slug}) {
  	try {
  		let response = await axios.get(process.env.musicApi + '/get/beat_by_slug/' + slug)

  		commit('FETCH_BEAT_SUCCESS', response.data.data)
  	} catch(e) {
  		console.log(e)
  		commit('FETCH_BEAT_FAILURE')
  	}
  }

}
