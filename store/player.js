import axios from 'axios'
import Cookies from 'js-cookie'

// state
export const state = () => ({
  show: false,
  playing: [],
  isPlaying: false,
  playlist: [],
  isPlaylistAvailable: false,
  recentPlays: [],
  playingArtiste: {},
  playingAlbum: {}
})

// getters
export const getters = {
  show: state => state.show,
  playing: state => state.playing,
  isPlaying: state => state.isPlaying,
  playlist: state => state.playlist,
  isPlaylistAvailable: state => state.isPlaylistAvailable,
  recentPlays: state => state.recentPlays,
  playingArtiste: state => state.playingArtiste,
  playingAlbum: state => state.playingAlbum
}

// mutations
export const mutations = {

  SET_PLAYER_STATE (state) {
    state.show = !state.show
  },

  SET_PLAYING (state, song) {
    state.playing.push(song)
  },

  FETCH_PLAYING_ARTIST_SUCCESS (state, artiste) {
    state.playingArtiste = artiste
  },

  FETCH_PLAYING_ARTIST_FAILURE (state) {
    state.playingArtiste = {}
  },

  FETCH_PLAYING_ALBUM_SUCCESS (state, album) {
    state.playingAlbum = album
  },

  FETCH_PLAYING_ALBUM_FAILURE (state) {
    state.playingAlbum = {}
  },

  NULL_PLAYING (state) {
    state.playing = []
  },

  SET_IS_PLAYING (state) {
    state.isPlaying = !state.isPlaying
  },

  SET_PLAYLIST (state, songs) {
    state.playlist = songs
  },

  SET_IS_PLAYLIST_AVAILABLE (state) {
    state.isPlaylistAvailable = !state.isPlaylistAvailable
  }
}

// actions
export const actions = {
  setPlayerVisibility ({ commit, dispatch }) {
    commit('SET_PLAYER_STATE')
  },

  setPlaying ({commit, dispatch}, song) {
    commit('NULL_PLAYING')
    commit('SET_PLAYING', song)
    dispatch('fetchArtist', {slug: song.artistSlug})
    dispatch('fetchAlbum', {slug: song.albumSlug})
  },

  setIsPlaying ({commit, dispatch}) {
    commit('SET_IS_PLAYING')
  },

  setPlaylist ({commit, dispatch}, songs) {
    commit('SET_PLAYLIST', songs)
    commit('SET_IS_PLAYLIST_AVAILABLE')
  },

  async fetchArtist ({commit, dispatch, state}, {slug}) {
    try {
      let response = await axios.get(process.env.musicApi + '/get/profile_by_slug/' + slug)

      commit('FETCH_PLAYING_ARTIST_SUCCESS', response.data.data)
    } catch(e) {
      console.log(e)
      commit('FETCH_PLAYING_ARTIST_FAILURE')
    }
  },

  async fetchAlbum ({commit, dispatch, state}, {slug}) {
    try {
      let response = await axios.get(process.env.musicApi + '/get/album_by_slug/' + slug)

      commit('FETCH_PLAYING_ALBUM_SUCCESS', response.data.data)
    } catch(e) {
      console.log(e)
      commit('FETCH_PLAYING_ALBUM_FAILURE')
    }
  }
}
