import axios from 'axios'
import Cookies from 'js-cookie'

// state
export const state = () => ({
  	blogs: [],
  	blog: {},
  	meta: {},
    categories: []
})

// getters
export const getters = {
  blogs: state => state.blogs,
  blog: state => state.blog,
  meta: state => state.meta,
  categories: state => state.categories
}

// mutations
export const mutations = {

  FETCH_BLOGS_SUCCESS (state, blogs) {
    state.blogs = blogs
  },

  FETCH_BLOGS_FAILURE (state) {
  	state.blogs = []
  },

  FETCH_MORE_SUCCESS (state, blogs) {
  	state.blogs = blogs
  },

  SET_FILTERED_BLOGS_SUCCESS (state, blogs) {
  	state.blogs = blogs
  },

  SET_FILTERED_BLOGS_FAILURE (state) {
  	state.blogs = []
  },

  SET_META (state, meta) {
  	state.meta = meta
  },

  FETCH_BLOG_SUCCESS (state, blog) {
  	state.blog = blog
  },

  FETCH_BLOG_FAILURE (state) {
  	state.blog = {}
  },

  FETCH_CATEGORIES_SUCCESS (state, categories) {
    state.categories = categories
  },

  FETCH_CATEGORIES_FAILURE (state) {
    state.categories = []
  },
}

// actions
export const actions = {
  async fetchBlogs ({commit, dispatch}) {
  	try {
      let response = await axios.get(process.env.musicApi + '/get/blogs')
      commit('FETCH_BLOGS_SUCCESS', response.data.data)
      commit('SET_META', response.data.meta.pagination)
    } catch (e) { 
    	commit('FETCH_BLOGS_FAILURE')
    }
  },

  async fetchMoreBlogs ({commit, dispatch, state}, {url}) {
  	try {
     let response =  await axios.get(url)
     commit('SET_META', response.data.meta.pagination)
     dispatch('mergeBlogs', {data: response.data.data})
    } catch (e) { 
    	console.log(e)
    	commit('FETCH_BLOGS_FAILURE')
    }
  },

  mergeBlogs ({commit, dispatch, state}, {data}) {
    try {
    	const result = state.blogs.concat(data)
    	commit('FETCH_MORE_SUCCESS', result)
    } catch(e) {
    	// statements
    	console.log(e)
    	commit('FETCH_BLOGS_FAILURE')
    }
  },

  filterBlogs ({commit, dispatch, state}, {letter}) {
  	try {
  		let filtereBlogs = state.blogs.filter(blog => {
           return blog.stageName.toLowerCase().indexOf(letter.toLowerCase()) > -1
    	})

    	commit('SET_FILTERED_BLOGS_SUCCESS', filtereBlogs)
  	} catch(e) {
  		console.log(e)
  		commit('SET_FILTERED_BLOGS_FAILURE')
  	}
  },

  async fetchBlog ({commit, dispatch, state}, {slug}) {
  	try {
  		let response = await axios.get(process.env.musicApi + '/get/blog_by_slug/' + slug)

  		commit('FETCH_BLOG_SUCCESS', response.data.data)
  	} catch(e) {
  		console.log(e)
  		commit('FETCH_BLOG_FAILURE')
  	}
  },

  async fetchCategories ({commit, dispatch}) {
    try {
      let response = await axios.get(process.env.musicApi + '/get/categories')
      commit('FETCH_CATEGORIES_SUCCESS', response.data.data)
    } catch (e) { 
      commit('FETCH_CATEGORIES_FAILURE')
    }
  },

}
