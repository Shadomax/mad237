import axios from 'axios'
import Cookies from 'js-cookie'

// state
export const state = () => ({
  	artiste: [],
  	artist: {},
  	meta: {},
  	filteredArtiste: []
})

// getters
export const getters = {
  artiste: state => state.artiste,
  artist: state => state.artist,
  meta: state => state.meta,
  filteredArtiste: state => state.filteredArtiste
}

// mutations
export const mutations = {

  FETCH_ARTISTE_SUCCESS (state, artiste) {
    state.artiste = artiste
  },

  FETCH_ARTISTE_FAILURE (state) {
  	state.artiste = []
  },

  FETCH_MORE_SUCCESS (state, artiste) {
  	state.artiste = artiste
  },

  SET_FILTERED_ARTISTE_SUCCESS (state, artiste) {
  	state.filteredArtiste = artiste
  },

  SET_FILTERED_ARTISTE_FAILURE (state) {
  	state.filteredArtiste = []
  },

  SET_META (state, meta) {
  	state.meta = meta
  },

  FETCH_ARTIST_SUCCESS (state, artist) {
  	state.artist = artist
  },

  FETCH_ARTIST_FAILURE (state) {
  	state.artist = {}
  }
}

// actions
export const actions = {
  async fetchArtiste ({commit, dispatch}) {
  	try {
      let response = await axios.get(process.env.musicApi + '/get/profiles')
      commit('FETCH_ARTISTE_SUCCESS', response.data.data)
      commit('SET_FILTERED_ARTISTE_SUCCESS', response.data.data)
      commit('SET_META', response.data.meta.pagination)
    } catch (e) { 
    	commit('FETCH_ARTISTE_FAILURE')
    }
  },

  async fetchMoreArtiste ({commit, dispatch, state}, {url}) {
  	try {
     let response =  await axios.get(url)
     dispatch('mergeArtiste', {data: response.data.data})
     commit('SET_META', response.data.meta.pagination)
    } catch (e) { 
    	console.log(e)
    	commit('FETCH_ARTISTE_FAILURE')
    }
  },

  mergeArtiste ({commit, dispatch, state}, {data}) {
    try {
    	const result = state.artiste.concat(data)
    	commit('FETCH_MORE_SUCCESS', result)
    } catch(e) {
    	// statements
    	console.log(e)
    	commit('FETCH_ARTISTE_FAILURE')
    }
  },

  filterArtiste ({commit, dispatch, state}, {letter}) {
  	try {
  		let filtereArtiste = state.artiste.filter(artist => {
           return artist.stageName.toLowerCase().indexOf(letter.toLowerCase()) > -1
    	})

    	commit('SET_FILTERED_ARTISTE_SUCCESS', filtereArtiste)
  	} catch(e) {
  		console.log(e)
  		commit('SET_FILTERED_ARTISTE_FAILURE')
  	}
  },

  async fetchArtist ({commit, dispatch, state}, {slug}) {
  	try {
  		let response = await axios.get(process.env.musicApi + '/get/profile_by_slug/' + slug)

  		commit('FETCH_ARTIST_SUCCESS', response.data.data)
  	} catch(e) {
  		console.log(e)
  		commit('FETCH_ARTIST_FAILURE')
  	}
  }

}
