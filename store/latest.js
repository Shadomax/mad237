import axios from 'axios'
import Cookies from 'js-cookie'

// state
export const state = () => ({
  	artiste: [],
  	blogs: [],
  	songs: [],
    promotions: []
})

// getters
export const getters = {
  artiste: state => state.artiste,
  blogs: state => state.blogs,
  songs: state => state.songs,
  promotions: state => state.promotions
}

// mutations
export const mutations = {

  FETCH_ARTISTE_SUCCESS (state, artiste) {
    state.artiste = artiste
  },

  FETCH_ARTISTE_FAILURE (state) {
  	state.artiste = []
  },

  FETCH_BLOGS_SUCCESS (state, blogs) {
  	state.blogs = blogs
  },

  FETCH_BLOGS_FAILURE (state) {
    state.blogs = []
  },

  FETCH_SONGS_SUCCESS (state, songs) {
  	state.songs = songs
  },

  FETCH_SONGS_FAILURE (state) {
  	state.songs = []
  },

  FETCH_PROMOTIONS_SUCCESS (state, songs) {
    state.promotions = songs
  },

  FETCH_PROMOTIONS_FAILURE (state) {
    state.promotions = []
  }
}

// actions
export const actions = {
  async fetchArtiste ({commit, dispatch}) {
  	try {
      let response = await axios.get(process.env.musicApi + '/get/latest_profiles')
      commit('FETCH_ARTISTE_SUCCESS', response.data.data)
    } catch (e) { 
    	commit('FETCH_ARTISTE_FAILURE')
    }
  },

  async fetchBlogs ({commit, dispatch}) {
    try {
      let response = await axios.get(process.env.musicApi + '/get/latest_blogs')
      commit('FETCH_BLOGS_SUCCESS', response.data.data)
    } catch (e) { 
      commit('FETCH_BLOGS_FAILURE')
    }
  },

  async fetchSongs ({commit, dispatch}) {
    try {
      let response = await axios.get(process.env.musicApi + '/get/latest_songs')
      commit('FETCH_SONGS_SUCCESS', response.data.data)
    } catch (e) { 
      commit('FETCH_SONGS_FAILURE')
    }
  },

  async fetchPromotions ({commit, dispatch}) {
    try {
      let response = await axios.get(process.env.musicApi + '/get/profiles')
      commit('FETCH_PROMOTIONS_SUCCESS', response.data.data)
    } catch (e) { 
      commit('FETCH_PROMOTIONS_FAILURE')
    }
  },

}
