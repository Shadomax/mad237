import axios from 'axios'
import Cookies from 'js-cookie'

// state
export const state = () => ({
  items: [],
  meta: {},
  item: {}
})

// getters
export const getters = {
  meta: state => state.meta,
  item: state => state.item,
  availableProducts: state => {
    return state.items.filter(product => product.inventory > 0)
  },

  productIsInStock: () => {
    return (product) => {
      return product.inventory > 0
    }
  }
}

// mutations
export const mutations = {

  FETCH_PRODUCTS_SUCCESS (state, products) {
    // update products
    state.items = products
  },

  FETCH_PRODUCTS_FAILURE (state) {
    state.items = []
  },

  SET_META (state, pagination) {
    state.meta = pagination
  },

  DECREMENT_PRODUCT_INVENTORY (state, payload) {
    payload.product.inventory -= payload.quantity
  },

  FETCH_PRODUCT_SUCCESS (state, product) {
    state.item = product
  },

  FETCH_PRODUCT_FAILURE (state) {
    state.item = {}
  }
}

// actions
export const actions = {
  fetchProducts({commit}) {
    return new Promise((resolve, reject) => {
      // make the call
      // call setProducts mutation
      axios.get(process.env.musicApi + '/get/products')
      .then(product => {
        commit('FETCH_PRODUCTS_SUCCESS', product.data.data)
        commit('SET_META', product.data.meta.pagination)
        resolve()
      })
    })
  },

  async fetchMoreProducts ({commit, dispatch, state}, {url}) {
    try {
     let response =  await axios.get(url)
     commit('SET_META', response.data.meta.pagination)
     console.log(response.data.meta.pagination)
     dispatch('mergeProducts', {data: response.data.data})
    } catch (e) { 
      console.log(e)
      commit('FETCH_PRODUCTS_FAILURE')
    }
  },

  mergeProducts ({commit, dispatch, state}, {data}) {
    try {
      const result = state.items.concat(data)
      commit('FETCH_PRODUCTS_SUCCESS', result)
    } catch(e) {
      // statements
      console.log(e)
      commit('FETCH_PRODUCTS_FAILURE')
    }
  },

  async fetchProduct ({commit, dispatch, state}, {slug}) {
    try {
      let response = await axios.get(process.env.musicApi + '/get/product_by_slug/' + slug)

      commit('FETCH_PRODUCT_SUCCESS', response.data.data)
    } catch(e) {
      console.log(e)
      commit('FETCH_PRODUCT_FAILURE')
    }
  }
}
