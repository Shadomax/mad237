// state
export const state = () => ({
  items: [],
  checkoutStatus: null
})

// getters
export const getters = {
  cartProducts: (state, getters, rootState, rootGetters) => {
    return state.items.map(cartItem => {
      if (cartItem.type === 'Product') {
        const item = rootState.product.items.find(item => item.id === cartItem.id)
        return {
          title: item.name,
          price: (cartItem.quantity * (parseFloat(item.price) * parseFloat(((100 - item.discount)/100))).toFixed(2)).toFixed(2),
          quantity: cartItem.quantity,
          beatPlan: cartItem.beatPlan,
          type: cartItem.type,
          size: cartItem.size,
          color: cartItem.color,
          unq_code: item.unq_code,
          id: cartItem.id,
          inventory: item.inventory,
          picture: item.pic
        }
      } else {
        const beatPlan = rootState.beat.beatPlans.filter(plan => plan.name === cartItem.beatPlan)
        const item = rootState.beat.beats.find(item => item.id === cartItem.id)
        return {
          title: item.name,
          price: (cartItem.quantity * (parseFloat(item.price) + parseFloat(beatPlan[0].price))).toFixed(2),
          quantity: cartItem.quantity,
          beatPlan: cartItem.beatPlan,
          type: cartItem.type,
          id: cartItem.id,
          picture: item.pic
        }
      }
    })
  },

  totalItem: state => Object.keys(state.items).length,

  cartTotal: (state, getters) => {
    return getters.cartProducts.reduce((total, item) => parseFloat(total) + parseFloat(item.price), 0)
  },
}

// mutations
export const mutations = {

  pushProductToCart (state, product) {
    state.items.push({
        id: product.id,
        quantity: product.quantity,
        type: product.type,
        color: product.color,
        size: product.size,
        beatPlan: product.beatPlan
      })
  },

  removeProductFromCart (state, productId) {
    let new_items = state.items.filter(item => {
      return item.id !== productId
    })

    state.items = new_items
  },

  incrementItemQuantity (state, cartItem) {
    cartItem.quantity++
  },

  setCheckoutStatus (state, status) {
    state.checkoutStatus = status
  },

  emptyCart (state) {
    state.items = []
  }
}

// actions
export const actions = {
  addProductToCart({state, getters, commit, rootState, rootGetters}, product) {
    try {
      if (product.type === 'Product') {
          if (rootGetters['product/productIsInStock'](product)) {
          const cartItem = state.items.find(item => item.id === product.id)
          if (!cartItem) {
            commit('pushProductToCart', product)
          } else {
            commit('incrementItemQuantity', cartItem)
          }
          commit('product/DECREMENT_PRODUCT_INVENTORY', {product: product, quantity: product.quantity}, {root: true})
        }
      } else {
        const cartItem = state.items.find(item => item.id === product.id)
        if (!cartItem) {
          commit('pushProductToCart', product)
        } else {
          commit('incrementItemQuantity', cartItem)
        }
      }
    } catch(e) {
      // statements
      console.log(e)
    }
  },

  removeProductFromCart ({state, getters, commit}, {productId}) {
    try {
      commit('removeProductFromCart', productId)
    } catch(e) {
      console.log(e)
    }
  },

  checkout({state, commit}) {
    shop.buyProducts(
      state.items,
      () => {
        commit('emptyCart')
        commit('setCheckoutStatus', 'success')
      },
      () => {
        commit('setCheckoutStatus', 'fail')
      }
    )
  }
}
