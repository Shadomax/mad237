const nodeExternals = require('webpack-node-externals')
const resolve = (dir) => require('path').join(__dirname, dir)
const webpack = require('webpack')
require('dotenv').config()

module.exports = {

  env: {
    apiUrl: process.env.API_URL || 'http://localhost:8000',
    appUrl: process.env.APP_URL || 'http://localhost:4000',
    appKeywords: process.env.APP_KEYWORDS || "Mad327",
    appName: process.env.APP_NAME || 'Mad327',
    musicApi: 'http://localhost:9000/api',
    appEmail: process.env.APP_EMAIL || 'info@mad237.com',
    appDeveloper: process.env.APP_DEVELOPER || 'Mbiarrambang Alain',
    appDeveloperEmail: process.env.APP_DEVELOPER_EMAIL || 'cosmasalain@gmail.com',
    appLocale: process.env.APP_LOCALE || 'en',
    githubAuth: !!process.env.GITHUB_CLIENT_ID
  },

  /*
  ** Headers of the page
  */
  head: {
    title: process.env.APP_NAME,
    titleTemplate: '%s - ' + process.env.APP_NAME,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '\"stream cameroonian music\"' },
      { hid: 'keywords', name: 'keywords', content: process.env.appKeywords },
      { hid: 'author', name: 'author', content: process.env.appDeveloper },
      { hid: 'ogtitle', property: 'og:title', name: 'og:title', content: 'Mad327' },
      { hid: 'ogimage', property: 'og:image', name: 'og:image', content: process.env.APP_URL + '/logo.png' },
      { hid: 'ogdescription', property: 'og:description', name: 'og:description', content: 'stream cameroonian music' },
      { hid: 'ogurl', property: 'og:url', name: 'og:url', content: process.env.APP_URL }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/logo.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://unpkg.com/vue-plyr@latest/dist/vue-plyr.css'}
    ],
    script: [
      { src: 'https://use.fontawesome.com/2e87b01d9d.js' }
    ]
  },
  plugins: [
    '~plugins/axios',
    '~plugins/filters',
    '~/plugins/vuetify.js',
    { src: '~/plugins/vue-notification', ssr: false },
    { src: '~/plugins/vue-notifications', ssr: false },
    { src: '~/plugins/vee-validate', ssr: true },
    { src: '~/plugins/vue-social-sharing', ssr: true },
    { src: '~/plugins/vue-lodash', ssr: false },
    { src: '~/plugins/vue-swal', ssr: false },
    { src: '~/plugins/vue-aplayer', ssr: false },
    { src: '~/plugins/vue-plyr', ssr: false },
    { src: '~/plugins/vue-ls', ssr: false },
    { src: '~/plugins/vue-events', ssr: false }
  ],
  css: [
    '~/assets/style/app.styl'
  ],

  modules: [
    '@nuxtjs/router'
  ],

  /*
  ** Customize the progress bar color
  */
  loading: { color: '#448AFF' },

  router: {
    middleware: ['check-auth']
  },
  
  /*
  ** Build configuration
  */
  build: {
    babel: {
      plugins: [
        ["transform-imports", {
          "vuetify": {
            "transform": "vuetify/es5/components/${member}",
            "preventFullImport": true
          }
        }]
      ]
    },
    vendor: [
      '~/plugins/vuetify.js',
      '~/plugins/vue-notification.js',
      '~/plugins/vue-notifications.js',
      '~/plugins/vee-validate.js',
      '~/plugins/vue-social-sharing.js',
      '~/plugins/vue-lodash.js',
      '~/plugins/vue-swal.js',
      '~/plugins/vue-plyr.js',
      '~/plugins/vue-ls.js',
      '~/plugins/vue-events.js'
    ],
    plugins: [
      new webpack.ProvidePlugin({
        '$': 'jquery',
        '_': 'lodash'
        // ...etc.
      })
    ],
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      // if (ctx.isDev && ctx.isClient) {
      //   config.module.rules.push({
      //     enforce: 'pre',
      //     test: /\.(js|vue)$/,
      //     loader: 'eslint-loader',
      //     exclude: /(node_modules)/
      //   })
      // }
      if (ctx.isServer) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/]
          })
        ]
      }
    }
  }
}
